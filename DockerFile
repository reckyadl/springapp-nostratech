# FROM maven:ibmjava-alpine AS build
# RUN mkdir /app
# COPY . /app
# WORKDIR /app
# RUN mvn clean package -DskipTests
# # RUN mvn clean install
# # ADD target/*.jar app.jar

# FROM openjdk:8-jdk-alpine
# ENV TZ Asia/Jakarta
# RUN addgroup -S spring && adduser -S spring -G spring
# USER spring:spring
# ARG JAR_FILE=/app/target/*.jar
# COPY --from=build ${JAR_FILE} app.jar
# ENTRYPOINT ["java", "-jar", "app.jar"]

FROM maven:ibmjava-alpine as build
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN mvn clean package -DskipTests

FROM openjdk:8-jdk-alpine
ENV TZ Asia/Jakarta
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=/app/target/*.jar
COPY --from=build $JAR_FILE app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
